import Vue from 'vue'
import Firebase from 'firebase'
import VueFire from 'vuefire'

const store = new Object(null)
const baseURL = 'https://kuza-staging.firebaseio.com/'

Vue.use(VueFire)

export default store

store.farms = new Firebase(baseURL + 'farms')
store.farmVisits = new Firebase(baseURL + 'farmVisits')
store.subscribers = new Firebase(baseURL + 'subscribers')
store.kits = new Firebase(baseURL + 'kits')
store.starterKitOrders = new Firebase(baseURL + 'kitOrders')