import Vue from 'vue'
import Router from 'vue-router'
import Validator from 'vue-validator'
import App from './components/App.vue'
import HomeView from './components/Home.vue'
import FarmsView from './components/Farms.vue'
import FarmView from './components/Farm.vue'
import KitView from './components/StarterKit.vue'


/* eslint-disable no-new */

Vue.use(Router)
Vue.use(Validator)

var router = new Router({
  hashbang: false,
  history: true,
  transitionOnLoad: true,
  root: '/',
  linkActiveClass: 'uk-active'
})


router.map({
  '/': {
    name: 'home',
    component: HomeView
  },
  '/farms': {
    name: 'farms',
    component: FarmsView
  },
  '/farms/:slug': {
    name: 'farm',
    component: FarmView
  },
  '/kit': {
    name: 'kit',
    component: KitView 
  }
})

router.beforeEach(function() {
  window.scrollTo(0, 0)
})

// redirect unknown routes to home page
router.redirect({
  '*': '/'
})

router.start(App, '#app')